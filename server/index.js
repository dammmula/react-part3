// import loginRoutes from './routes/login-routes.js';
const loginRoutes = require('./routes/login-routes');
const chatRoutes = require('./routes/chat-routes');
const userListRoutes = require('./routes/user-list-routes');
const userEditorRoutes = require('./routes/user-editor-routes');

const express = require("express");
const cors = require('cors');

const PORT = process.env.PORT || 3001;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/login", loginRoutes);
app.use("/chat", chatRoutes);
app.use("/user-list", userListRoutes);
app.use("/user-editor", userEditorRoutes)

// app.get("*", (req, res) => {
//     res.json({ message: "Hello from server!" });
//     res.redirect('/login');
// });

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});