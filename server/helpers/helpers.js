const findUser = (data, body) => {
    const {login, password} = body;
    const user = data.find((item) => {
        return item.login === login && item.password === password;
    });
    return user;
}

const deleteMessage = (messages, id) => {
    return messages.filter((message) => message.id !== id);
}

const editMessage = (messages, id, text) => {
    return messages.map((message) => {
        if (message.id === id) {
            message.text = text;
        }
        return message;
    });
}

const updateUsers = (users, user, prevLogin) => {
    const {login, password} = user;
    return users.map((user) => {
        if (user.login === prevLogin) {
            user.login = login;
            user.password = password;
        }
        return user;
    })
}

module.exports = {
    findUser,
    deleteMessage,
    editMessage,
    updateUsers
}