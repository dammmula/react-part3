const Router = require("express");
const fs = require("fs");
const {deleteMessage, editMessage} = require('../helpers/helpers');

const filePath = 'server/database.json';

const router = Router();

router.get('', (req, res) => {
    try {
        fs.readFile(filePath, "utf8", function(error,data) {
            const database = JSON.parse(data);

            res.json(database.messages);
        });
    } catch (error) {
        res.err = error;
    }
});

router.put('', (req, res) => {
    try {
        fs.readFile(filePath, "utf8", function(error,data) {
            const database = JSON.parse(data);
            database.messages.push(req.body.message);

            fs.writeFileSync(filePath, JSON.stringify(database));
            res.json(database.messages);
        });
    } catch (error) {
        res.err = error;
    }
})

router.delete('', (req, res) => {
    try {
        fs.readFile(filePath, "utf8", function(error,data) {
            const database = JSON.parse(data);
            database.messages = deleteMessage(database.messages, req.body.id);

            fs.writeFileSync(filePath, JSON.stringify(database));

            res.json(database.messages);
        });
    } catch (error) {
        res.err = error;
    }
})

router.post('', (req, res) => {
    try {
        fs.readFile(filePath, "utf8", function(error,data) {
            const database = JSON.parse(data);
            database.messages = editMessage(database.messages, req.body.id, req.body.text);

            fs.writeFileSync(filePath, JSON.stringify(database));

            res.json(database.messages);
        });
    } catch (error) {
        res.err = error;
    }
});

module.exports = router;
