const Router = require("express");
const fs = require("fs");
const {findUser} = require("../helpers/helpers");
const filePath = 'server/database.json';

const router = Router();

router.post('', (req, res) => {
    if (!req.body) {
        res.sendStatus(400);
    }
    try {
        fs.readFile(filePath, "utf8", function(error,data) {
            const database = JSON.parse(data);
            const user = findUser(database.users, req.body);

            if (!user) {
                res.json({ message: "Not found" });
            }

            res.json(user);
        });
    } catch (error) {
        res.err = error;
    }
});

module.exports = router;
