const Router = require("express");
const fs = require("fs");
const filePath = 'server/database.json';

const router = Router();

router.get('', (req, res) => {
    try {
        fs.readFile(filePath, "utf8", function(error,data) {
            const database = JSON.parse(data);

            res.json(database.users);
        });
    } catch (error) {
        res.err = error;
    }
});

module.exports = router;
