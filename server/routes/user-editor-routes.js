const Router = require("express");
const fs = require("fs");
const {findUser, updateUsers} = require("../helpers/helpers");

const filePath = 'server/database.json';

const router = Router();

router.put('', (req, res) => {
    try {
        fs.readFile(filePath, "utf8", function(error,data) {
            const database = JSON.parse(data);
            const user = req.body.user;
            const foundUser = findUser(database.users, user);
            if (foundUser) {
                res.json({"message": "User already exists"});
            } else {
                database.users.push(user);

                fs.writeFileSync(filePath, JSON.stringify(database));
                res.json(database.users);
            }
        });
    } catch (error) {
        res.err = error;
    }
});


router.post('', (req, res) => {
    try {
        console.log('post');
        fs.readFile(filePath, "utf8", function(error,data) {
            const database = JSON.parse(data);
            const users = updateUsers(database.users, req.body.user, req.body.prevLogin);
            database.users = users;

            fs.writeFileSync(filePath, JSON.stringify(database));
            res.json(database.users);
        });
    } catch (error) {
        res.err = error;
    }
})

module.exports = router;
