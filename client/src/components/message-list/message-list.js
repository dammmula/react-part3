import React from "react";
import Message from "../message/message";
import './message-list.css';
import OwnMessage from "../own-message/own-message";
import moment from "moment";

const MessageList = (props) => {
    const { messages, userId,
        onEditMessage, onDeleteMessage,
        onShowMessageEditor } = props;
    const today = moment();

    const elements = [];
    messages.map((item, index, array) => {
        const messageDate = moment(item.createdAt);
        const prevMessageDate = moment(array[index - 1]?.createdAt);

        if (messageDate.isAfter(prevMessageDate, 'day') || !array[index-1]) {
            let label;

            if (messageDate.isSame(today, 'day')) {
                label = 'Today';
            } else if (today.clone().subtract(1, 'days').isSame(messageDate, 'day')) {
                label = 'Yesterday';
            } else {
                label = messageDate.format('dddd, DD MMMM');
            }
            elements.push(
                <div className='messages-divider'
                     key={`${item.createdAt}${label}`}>
                    <span className="badge bg-dark">{label}</span>
                </div>
            );
        }

        if (item.userId === userId) {
            elements.push(<OwnMessage {...item}
                                      key={item.id}
                                      onShowMessageEditor={onShowMessageEditor}
                                      // onShowEditModal={onShowMessageEditor}
                                      onEditMessage={onEditMessage}
                                      onDeleteMessage={onDeleteMessage} />);
        } else {
            elements.push(<Message {...item} key={item.id}/>);
        }

        return item;
    })

    return (
        <div className='message-list card'>
            <div className='card-body'>
                { elements }
            </div>
        </div>
    );
}

export default MessageList;
