import React, {useEffect} from 'react';
import './user-list.css';
import * as actions from "../../store/actions/user-list-actions";
import {connect} from "react-redux";
import Preloader from "../preloader/preloader";
import {Link, Redirect} from "react-router-dom";

const UserList = (props) => {
    const {renderUsers, showUserEditor} = props;
    const {users, loading, editUser} = props.userList;

    useEffect(() => {
        renderUsers();
    }, [])

    const onEditUser = (login, password) => {
        showUserEditor(login, password);
    }

    const list = users.map((user) => {
        return (
            <User user={user}
                  onEditUser={onEditUser} />);
    });


    if (loading) {
        return <Preloader />;
    }

    if (editUser) {
        return <Redirect to='/user-editor' />;
    }

    return (
        <>
            <div className='user-list'>
                <div className="card">
                    <ul className="list-group list-group-flush">
                        {list}
                    </ul>
                </div>
            </div>
            <div className='links'>
                <Link to='/chat'>Chat</Link>
                <Link to='/user-editor'>Add user</Link>
            </div>
        </>

    );
}

const User = (props) => {
    const {onEditUser} = props;
    const {login, password} = props.user;
    const onBtnClick = () => {
        onEditUser(login, password);
    }

    return (
        <li className="list-group-item"
            key={login}>
            <div>
                Login: {login}
            </div>
            <div>
                Password: {password}
            </div>
            <button type="button"
                    className="btn btn-dark"
                    onClick={onBtnClick}>
                Edit user
            </button>
        </li>
    )
}

const mapStateToProps = (state) => {
    return {
        userList: state.userListReducer
    }
}

const mapDispatchToProps = {
    ...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList);