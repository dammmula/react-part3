import React, {useEffect, useState} from "react";
import './message-editor.css';
import {Link, Redirect} from "react-router-dom";
import * as actions from "../../store/actions/message-editor-actions";
import {connect} from "react-redux";
import Preloader from "../preloader/preloader";

const MessageEditor = (props) => {
    // const [messageEditorText, setMessageEditorText] = useState();

    const {onCloseModal, editMessageId, editMessage,
        editMessageText, messageEditorInput} = props;

    const {loading, input, edited} = props.messageEditor;
    // messageEditorInput(input);
    useEffect(() => {
        messageEditorInput(editMessageText)
    }, [])

    const onMessageEditorInput = (event) => {
        const text = event.target.value;
        messageEditorInput(text);
    }

    const onEditMessage = () => {
        editMessage(editMessageId, input);
    }

    if (loading) {
        return <Preloader />;
    }

    if (edited) {
        return <Redirect from='/message-editor' to='/chat'/>
    }

    return (
        <div className='edit-message-modal'>
            <div className="modal-content border-dark" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-header">
                    <h5 className="modal-title" id="staticBackdropLabel">Edit message</h5>
                    <Link to='/chat'>
                        <button type="button"
                                className="btn-close"
                                data-dismiss="modal"
                                onClick={onCloseModal}
                        />
                    </Link>
                </div>
                <div className="modal-body">
                        <textarea type="text"
                                  value={input}
                                  onChange={onMessageEditorInput}
                                  className="form-control edit-message-input"
                                  aria-label="Message"
                                  aria-describedby="button-addon2"
                        />
                </div>
                <div className="modal-footer">
                    <button
                        type="button"
                        className="btn btn-primary edit-message-button"
                        onClick={onEditMessage}>
                        OK
                    </button>
                    <button
                        type="button"
                        className="btn btn-secondary edit-message-close"
                        data-dismiss="modal"
                        onClick={onCloseModal}>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    );
}
const mapStateToProps = (state) => {
    return {
        messageEditor: state.messageEditorReducer
    }
}

const mapDispatchToProps = {
    ...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageEditor);
