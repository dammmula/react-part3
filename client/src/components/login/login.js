import React, {useState} from "react";
import {Redirect} from 'react-router-dom'
import './login.css';
import * as actions from "../../store/actions/login-actions";
import {connect} from "react-redux";
import Preloader from "../preloader/preloader";

const Login = (props) => {
    const {fetchUser, showWarning, setIsAdmin} = props;
    const {warning, user, loading} = props.login;

    const [loginInput, setLoginInput] = useState('');
    const [passwordInput, setPasswordInput] = useState('');

    const onLoginChange = (event) => {
        const text = event.target.value;
        setLoginInput(text);
    }

    const onPasswordChange = (event) => {
        const text = event.target.value;
        setPasswordInput(text);
    }

    const onButtonClick = (event) => {
        event.preventDefault();
        if (!loginInput.trim() || !passwordInput) {
            showWarning('Login or password is empty');
        } else {
            fetchUser({
                login: loginInput,
                password: passwordInput
            });
        }
    }



    if (user === 'admin') {
        setIsAdmin(true);
        return <Redirect to='/user-list' />;
    }

    if (user === 'user') {
        return <Redirect to='/chat' />;
    }

    if (loading) {
        return <Preloader />;
    }

    const warningLabel = warning ?
        <div id="passwordHelpBlock"  className="form-text">{warning}</div> :
        null;

    return (
        <div className='login'>
            <form>
                <div className="mb-3">
                    <label className="form-label">Login</label>
                    <input
                        className="form-control"
                        value={loginInput}
                        onChange={onLoginChange}/>
                </div>
                <div className="mb-3">
                    <label className="form-label">Password</label>
                    <input
                        type="password"
                        className="form-control"
                        value={passwordInput}
                        onChange={onPasswordChange}/>
                    {warningLabel}
                </div>

                <button type="submit"
                        className="btn btn-primary"
                        onClick={(e => onButtonClick(e))}>Submit</button>
            </form>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        login: state.loginReducer
    }
}

const mapDispatchToProps = {
    ...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
