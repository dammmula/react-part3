import React from "react";
import './preloader.css';

const Preloader = (props) => {
    return (
        <div className='preloader'>
            <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
            </div>
        </div>
    );
}

export default Preloader;
