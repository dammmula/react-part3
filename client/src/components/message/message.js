import React, {useState} from "react";
import './message.css';
import moment from "moment";

const Message = (props) => {
    const [isLiked, changeLike] = useState(false);
    const { avatar, user, text, createdAt, editedAt } = props;

    const onLikeClick = () => {
        changeLike(!isLiked);
    }

    const formatDate = (str) => {
        const date = moment(str).format('HH:mm');
        return date;
    }

    const likeClassName = isLiked ?
        'message-liked fas fa-heart' :
        'message-like far fa-heart';
    const edited = editedAt ? ' (edited)' : null;
    const time = formatDate(createdAt);

    return (
        <div className='message'>
            <div className=''>
                <img className='message-user-avatar' src={avatar} alt=''/>
            </div>

            <div className="card border-dark mb-3">
                <div className="card-body">
                    <h4 className="card-title message-user-name">{user}</h4>
                    <p className="card-text message-text">{text}</p>
                </div>
                <div className="card-footer bg-transparent">
                    <span className='message-time'>{ time } { edited }</span>
                    <i className={likeClassName} onClick={onLikeClick}></i>
                </div>
            </div>
        </div>
    );

}

export default Message;