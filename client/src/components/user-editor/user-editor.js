import React, {useState} from "react";
import './user-editor.css';
import {Link} from "react-router-dom";
import * as actions from "../../store/actions/user-list-actions";
import {connect} from "react-redux";
import {updateUser} from "../../store/actions/user-list-actions";

const UserEditor = (props) => {
    const {addUser, updateUser} = props;
    const {message, editedUserLogin, editedUserPassword, editUser} = props.userList;

    const [loginInput, setLoginInput] = useState(editedUserLogin);
    const [passwordInput, setPasswordInput] = useState(editedUserPassword);

    const onLoginInput = (event) => {
        const text = event.target.value;
        setLoginInput(text);
    }
    const onPasswordInput = (event) => {
        const text = event.target.value;
        setPasswordInput(text);
    }
    const onSubmit = (event) => {
        event.preventDefault();
        if (!loginInput.trim() || !passwordInput) {
            alert('Login or password is empty');
        } else {
            debugger
            if (editUser) {
                updateUser({
                        login: loginInput,
                        password: passwordInput
                    }, editedUserLogin
                );
            } else {
                addUser({
                    login: loginInput,
                    password: passwordInput
                })
            }
        }
        setPasswordInput('');
        setLoginInput('');
    }

    const alertLabel = message ? (
        <div className="alert alert-success d-flex align-items-center" role="alert">
            <div>
                Success!
            </div>
        </div> ) : null;

    const heading = editUser ?
        'Edit user' :
        'Add user'

    return (
        <div className='user-editor'>
            <div className='links'>
                <Link to='/user-list'>User list</Link>
                <Link to='/chat'>Chat</Link>
            </div>
            <form className='card'>
                <h5>{heading}</h5>

                <div className="mb-3">
                    <label className="form-label">Enter login</label>
                    <input className="form-control"
                           value={loginInput}
                           onChange={onLoginInput} />
                </div>
                <div className="mb-3">
                    <label className="form-label">Enter password</label>
                    <input className="form-control"
                        value={passwordInput}
                        onChange={onPasswordInput} />
                </div>
                <button
                    type="submit"
                    className="btn btn-primary"
                    onClick={onSubmit}>Submit</button>
            </form>
            {alertLabel}
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        userList: state.userListReducer
    }
}

const mapDispatchToProps = {
    ...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(UserEditor);

