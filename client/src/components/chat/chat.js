import React, {useEffect} from "react";
import Preloader from "../preloader/preloader";
import Header from "../header/header";
import MessageList from "../message-list/message-list";
import MessageInput from "../message-input/message-input";
import './chat.css';
import moment from "moment";
import * as actions from "../../store/actions/chat-actions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";


const Chat = (props) => {
    const {renderMessages, deleteMessage,
        inputMessage, sendMessage,
        editMessage, editLastMessage, setEditMessageId,
        setEditMessageText, isAdmin} = props;

    const {messages, preloader, messageInput,
        userId, editModal} = props.chat;

    useEffect(() => {
        document.addEventListener('keyup', (event) => {
            if (event.keyCode === 38) {
                editLastMessage();
            }
        })

        renderMessages();
    }, []);

    const createMessage = (text) => {
        return {
            text,
            createdAt: moment().format(),
            editedAt: '',
            userId: userId,
            id: moment().format()
        }
    }

    const onMessageInput = (event) => {
        const text = event.target.value;
        inputMessage(text);
    }

    const onMessageSend = () => {
        const newMessage = createMessage(messageInput);
        sendMessage(newMessage);
    }

    const onShowMessageEditor = (id, text) => {
        setEditMessageId(id);
        setEditMessageText(text);
    }

    const onEditMessage = () => {
        editMessage();
    }

    const onDeleteMessage = (id) => {
        deleteMessage(id);
    }


    if (preloader) {
        return <Preloader />;
    }

    const links = isAdmin ?
        <div className='links'><Link to='/user-list'>User list</Link></div> :
        null;

    const chatClassName = editModal ?
        'chat container faded' :
        'chat container';

    return (
        <>
            {links}
            <div className={chatClassName}>
                <Header messages={messages}/>
                <MessageList
                    onShowMessageEditor={onShowMessageEditor}
                    onEditMessage={onEditMessage}
                    onDeleteMessage={onDeleteMessage}
                    messages={messages}
                    userId={userId}/>
                <MessageInput
                    text={messageInput}
                    onMessageInput={onMessageInput}
                    onMessageSend={onMessageSend} />
            </div>
        </>

    );
}

const mapStateToProps = (state) => {
    return {
        chat: state.chatReducer.chat
    }
}

const mapDispatchToProps = {
    ...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);