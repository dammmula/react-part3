import React, {useEffect, useState} from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import "bootswatch/dist/lux/bootstrap.min.css";
import './app.css';
import Chat from "../chat/chat";
import Login from "../login/login";
import UserList from "../user-list/user-list";
import MessageEditor from "../message-editor/message-editor";
import UserEditor from "../user-editor/user-editor";

function App() {

    const [editMessageId, setEditMessageId] = useState(false);
    const [editMessageText, setEditMessageText] = useState(false);
    const [isAdmin, setIsAdmin] = useState(null);

    return (
        <div className="App">
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Login setIsAdmin={setIsAdmin}/>
                    </Route>
                    <Route path="/chat">
                        <Chat
                            isAdmin={isAdmin}
                            setEditMessageId={setEditMessageId}
                            setEditMessageText={setEditMessageText}/>
                    </Route>
                    <Route path="/user-list">
                        <UserList />
                    </Route>
                    <Route path="/message-editor">
                        <MessageEditor
                            editMessageId={editMessageId}
                            editMessageText={editMessageText}/>
                    </Route>
                    <Route path="/user-editor">
                        <UserEditor />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;

