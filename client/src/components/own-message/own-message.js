import React from "react";
import moment from "moment";
import './own-message.css';
import {Link} from "react-router-dom";

const OwnMessage = (props) => {
    const {id, text, onShowMessageEditor,
        onDeleteMessage, createdAt} = props;
    const time = moment(createdAt).format('HH:mm');

    return (
        <div className='own-message'>
            <div className="card border-dark mb-3">
                <div className="card-body">
                    <h4 className="card-title ">You</h4>
                    <p className="card-text message-text">{text}</p>
                </div>
                <div className="card-footer bg-transparent">
                        <span className='buttons'>
                            <Link to='/message-editor'
                                  onClick={() => onShowMessageEditor(id, text)}>
                                <i className="fas fa-cog message-edit"></i>
                            </Link>
                            <i className="fas fa-trash-alt message-delete"
                               onClick={() => onDeleteMessage(id)}></i>
                        </span>
                    <span className='message-time'>{time}</span>
                </div>
            </div>
        </div>
    )
}

export default OwnMessage;
