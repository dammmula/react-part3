import {applyMiddleware, createStore} from "redux";
import rootReducer from "./reducers/root-reducer";
import { composeWithDevTools } from 'redux-devtools-extension';
import {configureStore, getDefaultMiddleware} from '@reduxjs/toolkit'
import thunk from "redux-thunk";

const middleware = getDefaultMiddleware({
    immutableCheck: false,
    serializableCheck: false,
    thunk: true,
});

export const store = configureStore({
    reducer: rootReducer,
    middleware,
    devTools: process.env.NODE_ENV !== 'production'
});

// const store = createStore(rootReducer,  composeWithDevTools(applyMiddleware(thunk)));

export default store;

