import {CLOSE_MESSAGE_EDITOR, MESSAGE_EDITOR_INPUT} from "../actions/message-editor-actions";


const initialState = {
    'input': '',
    'loading': false,
    'edited': false
};

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case MESSAGE_EDITOR_INPUT:
            return {
                ...state,
                input: action.payload.text
            }
        case CLOSE_MESSAGE_EDITOR:
            return {
                ...state,
                edited: true
            }

        default: return state;
    }
}