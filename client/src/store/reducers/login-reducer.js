import {SET_USER, SHOW_WARNING, START_LOADING} from "../actions/login-actions";


const initialState = {
    'loading': false,
    'user': null,
    'warning': '',
};

export default function reducer(state = initialState, action) {

    switch(action.type) {
        case START_LOADING:
            return {
                ...state,
                loading: true
            }
        case SET_USER:
            return {
                ...state,
                user: action.payload.user,
                loading: false
            };
        case SHOW_WARNING:
            return {
                ...state,
                warning: action.payload.warning
            }
        default: return state;
    }
}