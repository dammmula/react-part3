import {EDIT_USER, SHOW_USERS, SHOW_PRELOADER, SHOW_USER_EDITOR, SHOW_ALERT} from "../actions/user-list-actions";

const initialState = {
    'users': [],
    'loading': false,
    'editUser': false,
    'message': false,
    'editedUserLogin': ''
};

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case SHOW_USERS:
            return {
                ...state,
                users: action.payload.users,
                loading: false
            }
        case SHOW_PRELOADER:
            return {
                ...state,
                loading: true
            }
        case SHOW_USER_EDITOR:
            return {
                ...state,
                editedUserLogin: action.payload.login,
                editedUserPassword: action.payload.password,
                editUser: true
            }
        case SHOW_ALERT:
            return {
                ...state,
                message: true
            }
        default: return state;
    }
}