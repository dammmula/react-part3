import chatReducer from './chat-reducer';
import loginReducer from './login-reducer';
import messageEditorReducer from './message-editor-reducer';
import userListReducer from './user-list-reducer';
import {combineReducers} from "redux";

// export {reducer as rootReducer} from './chat-reducer';


const rootReducer = combineReducers({
    chatReducer,
    loginReducer,
    messageEditorReducer,
    userListReducer
});

export default rootReducer;