import moment from "moment";

export const sortMessages = (messages) => {
    messages.sort(({createdAt: a}, {createdAt: b}) => {
        return moment(a) - moment(b);
    });
    return messages;
}

export const makeRequest = async (url, method = 'GET', body = null) => {
    try {
        const res = await fetch(url, {
            method,
            // body: body ? JSON.stringify(body) : undefined ,
            headers: { "Content-Type": 'application/json;charset=utf-8' }
        });

        const dataObj = await res.json();

        if(res.ok) {
            return dataObj;
        }

        alert(`${dataObj.message}`);
        return dataObj;
    } catch (err) {
        console.error(err);
    }
}