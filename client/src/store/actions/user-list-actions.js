export const SHOW_USERS = 'SHOW_USERS';
export const SHOW_PRELOADER = 'SHOW_PRELOADER';
export const SHOW_USER_EDITOR = 'SHOW_USER_EDITOR';
export const EDIT_USER = 'EDIT_USER';
export const SHOW_ALERT = 'SHOW_ALERT';

export const renderUsers = () => {
    return async (dispatch) => {
        dispatch(showPreloader);
        try {
            const res = await fetch('/user-list', {
                method: 'GET',
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                }
            });

            if (!res.ok) {
                throw new Error(`Could not fetch, received ${res.status}`);
            }

            const data = await res.json();
            dispatch(showUsers(data));

        } catch (error) {
            alert(error);
        }
    }
}

export const showPreloader = () => ({
    type: SHOW_PRELOADER
})

export const showUsers = (users) => ({
    type: SHOW_USERS,
    payload: {
        users
    }
})

export const showUserEditor = (login, password) => ({
    type: SHOW_USER_EDITOR,
    payload: {
        login,
        password
    }
})

export const addUser = (user) => {
    return async (dispatch) => {
        dispatch(showPreloader);
        try {
            const res = await fetch('/user-editor', {
                method: 'PUT',
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({user})
            });

            if (!res.ok) {
                throw new Error(`Could not fetch, received ${res.status}`);
            }


            const data = await res.json();

            if (data.message) {
                alert(data.message);
            } else {
                dispatch(showAlert());
            }

        } catch (error) {
            alert(error);
        }
    }
};

export const updateUser = (user, prevLogin) => {
    return async (dispatch) => {
        dispatch(showPreloader);
        try {
            const res = await fetch('/user-editor', {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({
                    prevLogin,
                    user
                })
            });

            if (!res.ok) {
                throw new Error(`Could not fetch, received ${res.status}`);
            }

            const data = await res.json();

            if (data.message) {
                alert(data.message);
            } else {
                dispatch(showAlert());
            }

        } catch (error) {
            alert(error);
        }
    }
};


export const showAlert = () => ({
    type: SHOW_ALERT,
})