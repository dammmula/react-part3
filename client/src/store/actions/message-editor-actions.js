import {sortMessages} from "../helpers/helpers";
import {saveMessages, showPreloader} from "./chat-actions";

export const MESSAGE_EDITOR_INPUT = 'MESSAGE_EDITOR_INPUT';
export const EDIT_MESSAGE = 'EDIT_MESSAGE';
export const CLOSE_MESSAGE_EDITOR = 'CLOSE_MESSAGE_EDITOR';

export const messageEditorInput = (text) => ({
    type: MESSAGE_EDITOR_INPUT,
    payload: {
        text
    }
})

export const editMessage = (id, text) => {
    return async (dispatch) => {
        dispatch(showPreloader);
        try {
            const res = await fetch('/chat', {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({ id, text })
            });

            if (!res.ok) {
                throw new Error(`Could not fetch, received ${res.status}`);
            }

            const data = await res.json();
            if (data) {
                dispatch(closeMessageEditor());
            }
        } catch (error) {
            alert(error);
        }
    }
}


export const closeMessageEditor = () => ({
    type: CLOSE_MESSAGE_EDITOR
})
