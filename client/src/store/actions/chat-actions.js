import {makeRequest, sortMessages} from "../helpers/helpers";

export const SAVE_MESSAGES = 'SAVE_MESSAGES';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export const INPUT_MESSAGE = 'INPUT_MESSAGE';
// export const SEND_MESSAGE = 'SEND_MESSAGE';
export const SHOW_EDIT_MODAL = 'SHOW_EDIT_MODAL'
export const INPUT_MODAL = 'INPUT_MODAL';
export const EDIT_MESSAGE = 'EDIT_MESSAGE';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const EDIT_LAST_MESSAGE = 'EDIT_LAST_MESSAGE';
export const OPEN_MESSAGE_EDITOR = 'OPEN_MESSAGE_EDITOR';
export const UPDATE_MESSAGES = 'UPDATE_MESSAGES';
export const SHOW_PRELOADER = 'SHOW_PRELOADER';
export const ON_MESSAGE_SEND = 'ON_MESSAGE_SEND';



export const renderMessages = () => {
    return async (dispatch) => {
        try {
            const res = await fetch('/chat', {
                method: 'GET',
                    headers: {
                    'Content-type': 'application/json;charset=utf-8'
                },
            });

            if (!res.ok) {
                throw new Error(`Could not fetch, received ${res.status}`);
            }

            const data = await res.json();
            dispatch(saveMessages(sortMessages(data)));
        } catch (error) {
            alert(error);
        }
    }
}

export const deleteMessage = (id) => {
    return async (dispatch) => {
        dispatch(showPreloader);
        try {
            const res = await fetch('/chat', {
                method: 'DELETE',
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({ id })
            });

            if (!res.ok) {
                throw new Error(`Could not fetch, received ${res.status}`);
            }

            const data = await res.json();
            dispatch(saveMessages(sortMessages(data)));
        } catch (error) {
            alert(error);
        }
    }
}

export const sendMessage = (message) => {
    return async (dispatch) => {
        dispatch(showPreloader);
        try {
            const res = await fetch('/chat', {
                method: 'PUT',
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({ message })
            });

            if (!res.ok) {
                throw new Error(`Could not fetch, received ${res.status}`);
            }

            const data = await res.json();
            dispatch(onMessageSend(sortMessages(data)))
        } catch (error) {
            alert(error);
        }
    }
}

export const showPreloader = () => ({
    type: SHOW_PRELOADER
})

export const saveMessages = (messages) => ({
    type: SAVE_MESSAGES,
    payload: {
        messages
    }
})

// export const deleteMessage = (id) => ({
//     type: DELETE_MESSAGE,
//     payload: {
//         id
//     }
// })

export const inputMessage = (text) => ({
    type: INPUT_MESSAGE,
    payload: {
        text
    }
})

export const onMessageSend = (messages) => ({
    type: ON_MESSAGE_SEND,
    payload: {
        messages
    }
})

export const openMessageEditor = (id) => ({
    type: OPEN_MESSAGE_EDITOR,
    payload: {
        id
    }
})
export const showEditModal = (id) => ({
    type: SHOW_EDIT_MODAL,
    payload: {
        id
    }
})

export const inputModal = (text) => ({
    type: INPUT_MODAL,
    payload: {
        text
    }
})

export const editMessage = () => ({
    type: EDIT_MESSAGE
})

export const closeModal = () => ({
    type: CLOSE_MODAL
})

export const editLastMessage = () => ({
    type: EDIT_LAST_MESSAGE
})