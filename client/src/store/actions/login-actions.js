import {sortMessages} from "../helpers/helpers";
import {saveMessages} from "./chat-actions";
import {createAction} from "@reduxjs/toolkit";

// export const SET_USER = createAction('SET_USER');
// export const SHOW_WARNING = createAction('SHOW_WARNING');
export const SET_USER = 'SET_USER';
export const SHOW_WARNING = 'SHOW_WARNING';
export const START_LOADING = 'START_LOADING';



export const fetchUser = (user) => {
    return async (dispatch) => {
        try {
            const res = await fetch('/login', {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify(user)
            });

            if (!res.ok) {
                throw new Error(`Could not fetch, received ${res.status}`);
            }

            const data = await res.json();

            if (data.message === 'Not found') {
                dispatch(showWarning('Wrong login or password'));
            } else if (data.login === 'admin') {
                dispatch(setUser('admin'));
            } else {
                dispatch(setUser('user'));
            }

        } catch (error) {
            console.warn(error);
        }
    }
}
export const startLoading = () => ({
    type: START_LOADING
})

export const setUser = (user) => ({
    type: SET_USER,
    payload: {
        user
    }
})

export const showWarning = (text) => ({
    type: SHOW_WARNING,
    payload: {
        warning: text
    }
})