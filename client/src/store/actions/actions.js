import * as chatActions from './chat-actions';
import * as loginActions from './login-actions';

export default {
    ...chatActions,
    ...loginActions
};